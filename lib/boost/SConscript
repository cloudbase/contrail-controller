# -*- mode: python; -*-

import os
import sys

cpu_count = 1
try:
    import multiprocessing
    cpu_count = multiprocessing.cpu_count()
except ImportError:
    pass
    
vpath = '#/third_party/boost_1_48_0'
engine = vpath + '/tools/build/v2/engine'
version = '.1.48.0'

def MapPath(xpath, list):
    return map(lambda x: '#/' + Dir('.').path + '/tools/' + x, list)

VariantDir('#/' + Dir('.').path + '/tools', engine, duplicate=0)

env = DefaultEnvironment().Clone()

yyacc = env.Program('yyacc0', MapPath(engine, ['yyacc.c']))

gram = env.Command(['jamgram.y', 'jamgram.h'], MapPath(engine, ['jamgram.yy']),
                   Dir('.').path + os.path.sep + 'yyacc0 $TARGETS $SOURCE')

env.Depends(gram, yyacc)

mkjambase = env.Program('mkjambase0', MapPath(engine, ['mkjambase.c']))

jambase = env.Command('jambase.c', MapPath(engine, ['Jambase']),
                      Dir('.').path + os.path.sep + 'mkjambase0 $TARGET $SOURCE')
env.Depends(jambase, mkjambase)

sources = [ 'command.c', 'compile.c', 'debug.c', 'expand.c', 'glob.c', 'hash.c',
            'hdrmacro.c', 'headers.c', 'jam.c', 'jamgram.c',
            'lists.c', 'make.c', 'make1.c', 'newstr.c', 'option.c', 'output.c',
            'parse.c', 'pathunix.c', 'pathvms.c', 'regexp.c',
            'rules.c', 'scan.c', 'search.c', 'subst.c', 'timestamp.c',
            'variable.c', 'modules.c', 'strings.c', 'filesys.c', 'builtins.c',
            'pwd.c', 'class.c', 'native.c', 'md5.c', 'w32_getreg.c',
            'modules/set.c', 'modules/path.c', 'modules/regex.c',
            'modules/property-set.c', 'modules/sequence.c', 'modules/order.c',
            'execunix.c', 'fileunix.c']

if sys.platform == 'win32':
    env.Append(CCFLAGS = '/DNT')
    for flag in ['/MD', '/MDd']:
        while env['CCFLAGS'].count(flag) > 0:
            env['CCFLAGS'].remove(flag)
    sources = sources[:-2] + ['execnt.c', 'filent.c', 'hcache.c']

srcs = MapPath(engine, sources)
srcs.append('jambase.c')
stage0 = env.Program('jam0', srcs)

if sys.platform == 'darwin':
    toolset = '--toolset=darwin'
elif sys.platform == 'win32':
    toolset = '--toolset=vc10 --layout=system'
else:
    toolset = '--toolset=gcc'

cmd = ('(cd ' + Dir(engine).abspath +
       ' && ' + Dir('.').abspath + os.path.sep + 'jam0 -f build.jam ' + toolset + ')')

#--toolset=darwin
#    '--extra-include=' + env['ENV']['SDKROOT'] + '/usr/include )')

if sys.platform != 'win32':
    bjam = env.Command('tools/bjam', engine + '/build.jam', cmd,
        ENV={'PATH': env['ENV']['PATH'],
             'LOCATE_TARGET': Dir('.').abspath + '/tools',
#            'SDKROOT': env['ENV']['SDKROOT']
        })
else:
    # WA for .mkdir from build.jam which does not work with full windows paths
    locate_target = Dir('.').abspath + os.path.sep + 'tools'

    engine_path = Dir(engine).abspath.split(os.path.sep)
    target_path = locate_target.split(os.path.sep)
    for (l, r) in zip(engine_path, target_path):
        if l != r:
            break
        engine_path.pop(0)
        target_path.pop(0)

    locate_target = os.path.sep.join(['..'] * (len(engine_path)) + target_path)
    bjam = env.Command('tools' + os.path.sep + 'bjam.exe',
                        engine + os.path.sep + 'build.jam', cmd,
                        ENV={'PATH': env['ENV']['PATH'],
                             'TEMP': env['ENV']['TEMP'],
                             'INCLUDE': env['ENV']['INCLUDE'],
                             'LIB': env['ENV']['LIB'],
                             'LOCATE_TARGET': locate_target})

env.Depends(bjam, stage0)

boost_libs = [
    'system', 'thread', 'date_time', 'regex', 'python', 'program_options',
    'filesystem', 'chrono'
]

jam_libs = ''
for xlib in boost_libs:
    if jam_libs:
        jam_libs += ' '
    jam_libs += '--with-' + xlib

# Fix for libicu on FreeBSD. We have to use self-compiled libicu and not the one
# from the system to ensure the same C++ ABI is used by all libs the Agent
# is linked against.
if sys.platform.startswith('freebsd'):
    cmd_opt = '-sICU_PATH=' + Dir('#/build').abspath
elif sys.platform == 'win32':
    cmd_opt = '--layout=system variant=' + env['BUILD_TYPE']
    if env['ENV']['PLATFORM'] == 'X64':
        cmd_opt += ' address-model=64'
else:
    cmd_opt = ''

#
# Use '--cxxflags=-option' to pass options to the compiler
#
build_cmd = ('(cd ' + Dir(vpath).abspath + ' && ' +
             Dir('.').abspath + '/tools/bjam ' +
             '-j' + str(cpu_count) + ' ' +
             '--build-dir=' + Dir('.').abspath + ' ' +
             '--prefix=' + Dir('#/build').abspath + ' ' +
             '--stagedir=' + Dir('#/build').abspath + ' ' +
             jam_libs + ' ' + cmd_opt + ' stage )')

generated_libs = map(lambda x: '#/build/lib/libboost_' + x + env['LIBSUFFIX'], boost_libs)
if sys.platform != 'win32':
    generated_libs += map(lambda x: '#/build/lib/libboost_' + x + env['SHLIBSUFFIX'] + version, boost_libs)
libs = env.Command(generated_libs, bjam, build_cmd,
                   ENV={'PATH': env['ENV']['PATH'],
                        'PROCESSOR_ARCHITECTURE': env['ENV']['PROCESSOR_ARCHITECTURE'],
                        'INCLUDE': env['ENV']['INCLUDE'],
                        'TEMP': env['ENV']['TEMP']
                   })

env.Depends(libs, bjam);

if sys.platform != 'win32':
    for lib in boost_libs:
        solib = '#/build/lib/libboost_' + lib + env['SHLIBSUFFIX'] + version
        solink = '#/build/lib/libboost_' + lib + env['LIBSUFFIX']
        env.Depends(solink, solib)
        env.Alias('install', env.Install(env['INSTALL_LIB'], solib))

libpath = Dir(vpath).abspath
if not os.path.exists(libpath):
    print '%s not present' % libpath
    sys.exit(1)

env.Symlink(Dir('#/build/include/boost'), Dir(vpath + '/boost'))

