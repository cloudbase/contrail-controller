/*
 * Copyright (c) 2013 Juniper Networks, Inc. All rights reserved.
 */

#include <assert.h>

//#include <bits/sockaddr.h>
//#include <linux/netlink.h>
//#include <linux/rtnetlink.h>
//#include <ifaddrs.h>

#include <base/logging.h>
#include <base/util.h>
#include <cmn/agent_cmn.h>
#include <init/agent_param.h>
#include <cfg/cfg_init.h>
#include <oper/route_common.h>
#include <oper/mirror_table.h>
#include <ksync/ksync_index.h>
#include <vrouter/ksync/interface_ksync.h>
#include "vnswif_listener.h"

VnswInterfaceListenerLinux::VnswInterfaceListenerLinux(Agent *agent) :
    VnswInterfaceListenerBase(agent) {
}

VnswInterfaceListenerLinux::~VnswInterfaceListenerLinux() {
}
int VnswInterfaceListenerLinux::CreateSocket() {
    return -1;
}

void VnswInterfaceListenerLinux::SyncCurrentState() {}

// Initiate netlink scan based on type and flags
void
VnswInterfaceListenerLinux::InitNetlinkScan(uint32_t type, uint32_t seqno) {
}


void VnswInterfaceListenerLinux::RegisterAsyncReadHandler() {
}


void VnswInterfaceListenerLinux::ReadHandler(
    const boost::system::error_code &error, std::size_t len) {
    struct nlmsghdr *nlh;

    if (error == 0) {
        nlh = (struct nlmsghdr *)read_buf_;
        NlMsgDecode(nlh, len, -1);
    } else {
        LOG(ERROR, "Error < : " << error.message() <<
                   "> reading packet on netlink sock");
    }

    if (read_buf_) {
        delete [] read_buf_;
        read_buf_ = NULL;
    }
    RegisterAsyncReadHandler();
}


/****************************************************************************
 * Link Local route event handler
 ****************************************************************************/
int VnswInterfaceListenerLinux::AddAttr(uint8_t *buff, int type, void *data, int alen) {
    return 0;
}

void VnswInterfaceListenerLinux::UpdateLinkLocalRoute(const Ip4Address &addr,
                                                 bool del_rt) {
}

/****************************************************************************
 * Netlink message handlers
 * Decodes netlink messages and enqueues events to revent_queue_
 ****************************************************************************/
string VnswInterfaceListenerLinux::NetlinkTypeToString(uint32_t type) {
    return "";
}

VnswInterfaceListenerBase::Event *
VnswInterfaceListenerLinux::HandleNetlinkRouteMsg(struct nlmsghdr *nlh) {
    return 0;
}

VnswInterfaceListenerBase::Event *
VnswInterfaceListenerLinux::HandleNetlinkIntfMsg(struct nlmsghdr *nlh) {
    return 0;
}

VnswInterfaceListenerBase::Event *
VnswInterfaceListenerLinux::HandleNetlinkAddrMsg(struct nlmsghdr *nlh) {
    return 0;
}

int VnswInterfaceListenerLinux::NlMsgDecode(struct nlmsghdr *nl,
                                            std::size_t len, uint32_t seq_no) {
    return 0;
}
