/*
 * Copyright (c) 2014 Juniper Networks, Inc. All rights reserved.
 */


#include <stdio.h>
#include <fcntl.h>
#include <assert.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <cstring>
#include <string>
#include <net/if.h>
#include <unistd.h>

#include <netinet/in.h>

#include <net/ethernet.h>
#include <sys/errno.h>

#include "base/os.h"
#include "base/logging.h"

#define TUN_INTF_CLONE_DEV "/dev/net/tun"

void DeleteTap(int fd) {
}

void DeleteTapIntf(const int fd[], int count) {
    for (int i = 0; i < count; i++) {
        DeleteTap(fd[i]);
    }
}

int CreateTap(const char *name) {
    return -1;
}

void CreateTapIntf(const char *name, int count) {
}

void CreateTapInterfaces(const char *name, int count, int *fd) {
}

