#
# Copyright (c) 2013 Juniper Networks, Inc. All rights reserved.
#

import os
import sys
import re
import time

Import('AgentEnv')
env = AgentEnv.Clone()

if sys.platform != 'win32':
    cflags = env['CCFLAGS']
    cflags.append('-Wno-return-type')
    cflags.append('-fexceptions')
    cflags.append('-Wno-unused-variable')


##########################################################################
# Rules to generate buildinfo.cc
##########################################################################

# Dependencies on external libraries first
buildinfo_dep_libs = [
    '#build/lib/libhttp' + env['LIBSUFFIX'],
    '#build/lib/libhttp_parser' + env['LIBSUFFIX'],
    '#build/lib/libpugixml' + env['LIBSUFFIX'],
    '#build/lib/libsandesh' + env['LIBSUFFIX'],
    '#build/lib/libsandeshflow' + env['LIBSUFFIX'],
    '#build/lib/libsandeshvns' + env['LIBSUFFIX']
]
if sys.platform != 'win32':
    buildinfo_dep_libs = [
        '#build/lib/libthrift' + env['LIBSUFFIX'],
        '#build/lib/libthriftasio' + env['LIBSUFFIX'],
    ]

def MapBuildCmnLib(list):
    return map(lambda x: File('../../../' + x).abspath, list)

# Common contrail libraries
buildinfo_dep_libs += MapBuildCmnLib([
    'base/libbase' + env['LIBSUFFIX'],
    'base/libcpuinfo' + env['LIBSUFFIX'],
    'db/libdb' + env['LIBSUFFIX'],
    'dns/bind/libbind_interface' + env['LIBSUFFIX'],
    'ifmap/libifmap_agent' + env['LIBSUFFIX'],
    'ifmap/libifmap_common' + env['LIBSUFFIX'],
    'io/libio' + env['LIBSUFFIX'],
    'ksync/libksync' + env['LIBSUFFIX'],
    'net/libnet' + env['LIBSUFFIX'],
    'route/libroute' + env['LIBSUFFIX'],
    'schema/libbgp_schema' + env['LIBSUFFIX'],
    'schema/libifmap_vnc' + env['LIBSUFFIX'],
    'schema/libxmpp_unicast' + env['LIBSUFFIX'],
    'schema/libxmpp_multicast' + env['LIBSUFFIX'],
    'vrouter/utils/libvrutil' + env['LIBSUFFIX'],
    'xml/libxml' + env['LIBSUFFIX'],
    'xmpp/libxmpp' + env['LIBSUFFIX']
     ])

def MapBuildAgentLib(list):
    return map(lambda x: File('../' + x).abspath, list)

# agent libraries
buildinfo_dep_libs += MapBuildAgentLib([
    'cfg/libcfg' + env['LIBSUFFIX'],
    'cmn/libvnswcmn' + env['LIBSUFFIX'],
    'controller/libvnswctrl' + env['LIBSUFFIX'],
    'filter/libfilter' + env['LIBSUFFIX'],
    'kstate/libkstate' + env['LIBSUFFIX'],
    'vrouter/ksync/libvnswksync' + env['LIBSUFFIX'],
    'port_ipc/libport_ipc' + env['LIBSUFFIX'],
    'oper/libvnswoperdb' + env['LIBSUFFIX'],
    'pkt/libpkt' + env['LIBSUFFIX'],
    'diag/libdiag' + env['LIBSUFFIX'],
    'services/libagent_services' + env['LIBSUFFIX'],
    'uve/libuve' + env['LIBSUFFIX'],
    'uve/libstatsuve' + env['LIBSUFFIX'],
    'vrouter/flow_stats/libflowstats' + env['LIBSUFFIX'],
    'vrouter/stats_collector/libstatscollector' + env['LIBSUFFIX']
])
if sys.platform != 'win32':
    buildinfo_dep_libs += MapBuildAgentLib([
        'openstack/libnova_ins' + env['LIBSUFFIX'],
        'openstack/libnova_ins_thrift' + env['LIBSUFFIX'],
    ])

env.GenerateBuildInfoCode(
    target = ['buildinfo.h', 'buildinfo.cc'],
    source = buildinfo_dep_libs + ['main.cc'],
    path=str(Dir('.').abspath))
env.Depends('main.o', 'buildinfo.cc')

# Add contrail-vrouter-agent specific libraries
env.Prepend(LIBS = AgentEnv['CONTRAIL_LIBS'])

# Whole-arhive flag causes the library to be included in the resulting output
# even though there may not be any calls requiring its presence. We need this
# for kstate library so that the HTML file for kstate  is listed in agent
# introspect page
if sys.platform != 'win32':
    env.Prepend(LINKFLAGS = ['-Wl,--whole-archive', '-rdynamic', '-lkstate', '-ldiag',
                             '-Wl,--no-whole-archive'])

#Add -Wno-return-type to ignore errors from thrift generated code in
#InstanceService.h
if sys.platform.startswith('freebsd'):
    platform_dependent = 'freebsd/pkt0_interface.cc'
elif sys.platform.startswith('linux'):
    platform_dependent = 'linux/pkt0_interface.cc'
elif sys.platform.startswith('win32'):
    platform_dependent = 'windows/pkt0_interface.cc'


contrail_vrouter_agent = env.Program(target = 'contrail-vrouter-agent',
                     source = ['buildinfo.cc',
                               'contrail_agent_init.cc',
                               'main.cc',
                               'pkt0_interface_base.cc',
                                platform_dependent])

env.Alias('contrail-vrouter-agent', contrail_vrouter_agent)
env.Default(contrail_vrouter_agent)

# Documentation
doc_files = []
doc_files += env['AGENT_COMMON_DOC_FILES']
doc_files += env['AGENT_UVE_DOC_FILES']
doc_files += env['AGENT_KSYNC_DOC_FILES']
doc_files += env['AGENT_FLOW_DOC_FILES']

# Please update sandesh/common/vns.sandesh on process name change
env.Alias('install', env.Install(env['INSTALL_BIN'], contrail_vrouter_agent))
env.Alias('install', env.Install(env['INSTALL_EXAMPLE'],
          'contrail-vrouter-agent-health-check.py'))
env.Alias('install', env.Install(env['INSTALL_CONF'],
          '../contrail-vrouter-agent.conf'))
env.Alias('install',
        env.InstallAs(env['INSTALL_CONF'] + 
        '/supervisord_vrouter_files/' + 'contrail-vrouter-agent.ini',
        '../contrail-vrouter-agent.ini'))
env.Alias('install',
        env.InstallAs(env['INSTALL_INITD'] + '/contrail-vrouter-agent',
        '../contrail-vrouter.initd.supervisord'))
env.Alias('install', env.Install(env['INSTALL_CONF'], 
    '../supervisord_vrouter.conf'))
env.Alias('install', env.Install(
    env['INSTALL_MESSAGE_DOC'] + '/contrail-vrouter-agent/',
    doc_files))

if sys.platform == 'win32':
    def install_config_file(target, source, env):
        with open(source[0].abspath, 'r') as src, open(target[0].abspath, 'w') as out:
            del_section = False
            mandatory_section = False
            for line in src:
                if line.lstrip().startswith('log_file'):
                    out.write('log_file=' + os.path.abspath(env['INSTALL_ROOT'] + '/log/vrouter.log') + '\n')
                elif line.lstrip().startswith('[SERVICE-INSTANCE]'):
                    del_section = True
                elif line.lstrip().startswith('[DISCOVERY]'):
                    mandatory_section = True
                    out.write(line)
                elif line.lstrip().startswith('[VIRTUAL-HOST-INTERFACE]'):
                    mandatory_section = True
                    out.write(line)
                else:
                    if line.lstrip().startswith('['):
                        del_section = False
                        mandatory_section = False
                    if mandatory_section and line.lstrip().startswith('#') and '=' in line:
                        out.write(line.lstrip(' #'))
                    elif not del_section:
                        out.write(line)

    def install_bat_file(target, source, env):
        with open(target[0].abspath, 'w') as out:
            out.write('set PATH=' + os.path.abspath(env['INSTALL_BIN']) + ';%PATH%\n')
            out.write('if not exist log mkdir log\n')
            out.write(os.path.abspath(env['INSTALL_BIN'] + '/contrail-vrouter-agent' + env['PROGSUFFIX']) +
                ' --config_file=' + os.path.abspath(env['INSTALL_CONF'] + '/contrail-vrouter-agent.conf') + '\n')

    env.Alias('win-install', env.Command(env['INSTALL_CONF'] + '/contrail-vrouter-agent.conf',
                                         '../contrail-vrouter-agent.conf', install_config_file))
    env.Alias('win-install', env.Command(env['INSTALL_CONF'] + '/contrail-vrouter-agent.bat',
                                         '', install_bat_file))

    env.Alias('win-install', env.Install(env['INSTALL_BIN'], contrail_vrouter_agent))
    env.Alias('win-install', env.Install(env['INSTALL_MESSAGE_DOC'] + '/contrail-vrouter-agent/', doc_files))
