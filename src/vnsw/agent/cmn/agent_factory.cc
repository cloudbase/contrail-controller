/*
 * Copyright (c) 2014 Juniper Networks, Inc. All rights reserved.
 */

#include "cmn/agent_factory.h"

template <>
AgentObjectFactory *Factory<AgentObjectFactory>::singleton_ = NULL;

#ifndef _WIN32
#include "cmn/agent_signal.h"
#endif

#include "oper/ifmap_dependency_manager.h"

#ifndef _WIN32
#include "oper/instance_manager.h"
#include "nexthop_server/nexthop_manager.h"
#endif

#ifndef _WIN32
FACTORY_STATIC_REGISTER(AgentObjectFactory, AgentSignal,
                        AgentSignal);
#endif

FACTORY_STATIC_REGISTER(AgentObjectFactory, IFMapDependencyManager,
                        IFMapDependencyManager);

#ifndef _WIN32
FACTORY_STATIC_REGISTER(AgentObjectFactory, InstanceManager, InstanceManager);
FACTORY_STATIC_REGISTER(AgentObjectFactory, NexthopManager,
                        NexthopManager);
#endif
